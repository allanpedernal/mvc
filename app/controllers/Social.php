<?php

class Social extends Controller {

    const API_KEY = 'AIzaSyAIZertV8gmEDFkTDLYuutZCb8YoA9xkNw';

    /*
    * URL: /social
    *
    * @return string
    */
    public function index()
    {
        // get data
        $API_key = self::API_KEY;
        $channelID  = self::CHANNEL_ID;
        $maxResults = 10;

        // construct url
        $url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&q=guy';
        //$url .= "?channelId=".$channelID;
        $url .= "&maxResults=".$maxResults;
        $url .= "&key=".$API_key;

        $videoList = json_decode(file_get_contents($url));

        foreach($videoList->items as $item){

            if(isset($item->id->videoId)){
                echo "
                    <div class='youtube-video'>
                        <iframe width='280' height='150' src='https://www.youtube.com/embed/".$item->id->videoId."' frameborder='0' allowfullscreen></iframe>
                        <h2>". $item->snippet->title ."</h2>
                    </div>
                ";
            }
        }
    }
}