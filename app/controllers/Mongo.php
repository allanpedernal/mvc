<?php

use \MongoDB\BSON\Regex;

class Mongo extends Controller 
{
    private $mongodb;

    // construct
    public function __construct()
    {
        $this->mongodb = $this->model('MongoDB');
    }

    /*
    * URL: /mongo/search/18cf11de9e5dd4d0422b1ca715c25ad3
    *
    * @params $name = '18cf11de9e5dd4d0422b1ca715c25ad3';
    * @return string
    */
    public function search($name = '')
    {
        try
        {
            if(! empty($name))
            {
                $name = new \MongoDB\BSON\Regex($name);
                $users = $this->mongodb->search(['name' => $name]);

                echo "<pre>";
                print_r($users->toArray());
                exit;
            }

            echo 'Missing Name!';
        }
        catch(Exception $e)
        {
            echo json_encode(['success'=>false, 'message'=>$e->getMessage()]);
            exit;
        }  
    }

    /*
    * URL: /mongo/get/625d4d364071eb0d35051ff5
    *
    * @params $_id = '625d4d364071eb0d35051ff5';
    * @return string
    */
    public function get($id)
    {
        try
        {
            $user = $this->mongodb->get($id);

            echo "<pre>";
            print_r($user);
            exit;
        }
        catch(Exception $e)
        {
            echo json_encode(['success'=>false, 'message'=>$e->getMessage()]);
            exit;
        } 
    }

    /*
    * URL: /mongo/store
    *
    * @return string
    */
    public function store()
    {
        try
        {
            $data = array();
            $data['name'] = substr(md5(mt_rand()), 0, 32);
            $data['description'] = substr(md5(mt_rand()), 0, 32);

            $this->mongodb->store($data);
            echo 'User is successfully added!';
            exit;
        }
        catch(Exception $e)
        {
            echo json_encode(['success'=>false, 'message'=>$e->getMessage()]);
            exit;
        }
    }

    /*
    * URL: /mongo/update/625d4d364071eb0d35051ff5
    *
    * @params $_id = '625d4d364071eb0d35051ff5';
    * @return string
    */
    public function update($id)
    {
        try
        {
            $data['name'] = substr(md5(mt_rand()), 0, 32);
            $data['description'] = substr(md5(mt_rand()), 0, 32);

            $this->mongodb->update($id, $data);
            echo 'User is successfully updated!';
            exit;
        }
        catch(Exception $e)
        {
            echo json_encode(['success'=>false, 'message'=>$e->getMessage()]);
            exit;
        }
    }

    /*
    * URL: /mongo/update/625d4d364071eb0d35051ff5
    *
    * @params $_id = '625d4d364071eb0d35051ff5';
    * @return string
    */
    public function delete($id)
    {
        try
        {
            $this->mongodb->delete($id);
            echo 'User is successfully deleted!';
            exit;
        }
        catch(Exception $e)
        {
            echo json_encode(['success'=>false, 'message'=>$e->getMessage()]);
            exit;
        }
    }
}