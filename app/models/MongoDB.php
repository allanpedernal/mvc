<?php
/*
 #
 # Base Controller
 # Loads the models and view
 #
 */

use MongoDB\Client;
use MongoDB\BSON\ObjectID;

class MongoDB {

    private $collection;

    public function __construct()
    {
        $mongodb = new Client('mongodb://localhost:27017');
        $db = $mongodb->selectDatabase('MVC');
        $this->collection = $db->selectCollection('users');
        return $this->collection;
    }

    // search by field
    public function search($params = array())
    {
        if(COUNT($params))
        {
            $results = $this->collection->find($params);
            return $results;
        }
    }

    // store record
    public function store($data = array())
    {
        if(COUNT($data))
        {
            $result = $this->collection->insertOne($data);
            return $result->getInsertedId();
        }
    }

    // get record
    public function get($id = '')
    {
        if(! empty($id))
        {
            $result = $this->collection->findOne(["_id" => new ObjectID($id)]);
            return $result;
        }
    }

    // update record
    public function update($id = '', $data = array())
    {
        if(! empty($id) && COUNT($data))
        {
            $result = $this->collection->updateOne(["_id" => new ObjectID($id)], ['$set' => $data]);
            return $result;
        }
    }

    // delete record
    public function delete($id = '')
    {
        if(! empty($id))
        {
            $result = $this->collection->deleteOne(["_id" => new ObjectID($id)]);
            return $result;
        }
    }
}